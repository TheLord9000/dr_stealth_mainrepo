﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class InteractiveObject : MonoBehaviour {

    public bool pickable;
    public bool pickedUp;
    public bool used=false;
    public InteractiveObject[] interactives;
    public string Name;
    
    public bool Use(InteractiveObject interactive)
    {
        MyGUI gui = FindObjectOfType<MyGUI>();
        if (Array.Exists(interactives, element => element.Name == interactive.Name))
        {
            gui.usedObject.text = Name + " used with " + interactive.Name;
            Debug.Log(Name + " used with " + interactive.Name);
            return true;
        }
        gui.usedObject.text = "You can't use " + Name + " with " + interactive.Name;
        Debug.Log("interaction not possible between " + Name + " and " + interactive.Name);
        return false;
    }
    public static InteractiveObject[] FindByName(String name)
    {
        InteractiveObject[] objects = FindObjectsOfType<InteractiveObject>();
        ArrayList result = new ArrayList();
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].Name == name)
            {
                result.Add(objects[i]);
            }
        }
        return result.ToArray(typeof(InteractiveObject)) as InteractiveObject[];
    }
}
