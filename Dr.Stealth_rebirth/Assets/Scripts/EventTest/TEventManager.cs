﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEventManager : MonoBehaviour
{
    public delegate void aaa();
    public static event aaa AaaEvent;

    public static void RaiseAaaEvent()
    {
        if ( AaaEvent != null )
            AaaEvent();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
