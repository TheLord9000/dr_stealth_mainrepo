﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TPlayMusic : MonoBehaviour
{
    private void Play()
    {
        voice.Play();
    }

    AudioSource voice;

    // Start is called before the first frame update
    void Start()
    {
        voice = GetComponent<AudioSource>();
        TEventManager.AaaEvent += Play;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        TEventManager.AaaEvent -= Play;
    }
}
