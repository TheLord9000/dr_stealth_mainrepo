using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TObjectScript : MonoBehaviour
{
    void Reaction()
    {
        transform.position = new Vector3(transform.position.x, 5, transform.position.z);
    }

    // Start is called before the first frame update
    void Start()
    {
        TEventManager.AaaEvent += Reaction;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnMouseDown()
    {
        TEventManager.RaiseAaaEvent();
    }

    private void OnDestroy()
    {
        TEventManager.AaaEvent -= Reaction;
    }
}
