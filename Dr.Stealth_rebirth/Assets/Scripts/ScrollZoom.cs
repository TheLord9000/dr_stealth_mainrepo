﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollZoom : MonoBehaviour
{
    private Camera mainCam;

    private void Start()
    {
        mainCam = Camera.main;
    }

    void Update()
    {
        float scrollValue = Input.GetAxis("Mouse ScrollWheel");
        if (scrollValue > 0f)
        {
            if (mainCam.orthographicSize > 2f) mainCam.orthographicSize -= 0.2f;
        }
        else if (scrollValue < 0f)
        {
            if (mainCam.orthographicSize < 8f) mainCam.orthographicSize += 0.2f;
        }
    }
}
