﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingSpot : MonoBehaviour {

    private Animator mainAnim;
    private bool waitForAnimation;

    private bool hidden;
    private Vector3 prevPos;
    private GameObject player;

    private void Start()
    {
        mainAnim = GetComponent<Animator>();        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            player = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(!hidden) player = null;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            player.GetComponent<PlayerController>().movementEnabled = false;
            mainAnim.Play("Wardrobe_door_open");
        }
    }

    private void Hide()
    {
        if(!hidden)
        {
            hidden = true;
            prevPos = player.transform.position;
            player.transform.position = transform.position + new Vector3(0f, 1f, 0f);
            player.GetComponent<Collider>().enabled = false;
            //player.GetComponentInChildren<Transform>().gameObject.SetActive(false);
            mainAnim.Play("Wardrobe_door_close");
        }
        else
        {
            player.GetComponent<Collider>().enabled = true;
            hidden = false;
            player.transform.position = prevPos;
            mainAnim.Play("Wardrobe_door_close");
            //player.GetComponentInChildren<Transform>().gameObject.SetActive(true);
            player.GetComponent<PlayerController>().movementEnabled = true;
        }
        
    }
}
