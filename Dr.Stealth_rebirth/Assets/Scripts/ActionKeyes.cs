﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ActionKeyes : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Level_01_Filip");
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            if (Waypoint.seeDetectionPoints == true) Waypoint.seeDetectionPoints = false;
            else Waypoint.seeDetectionPoints = true;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    }
}
