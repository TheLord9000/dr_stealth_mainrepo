﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class _MainEventManager : MonoBehaviour
{
    public static event Action plyerDetected;

    public static IEnumerator RaisePlyerDetectedEvent( float delay=0 )
    {
        if (plyerDetected != null)
        {
            yield return new WaitForSeconds(delay);
            plyerDetected();
        }
    }


    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
