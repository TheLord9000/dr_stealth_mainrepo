﻿using UnityEngine;

/// <summary>
/// Simple billboard script used to clone camera rotation to object.
/// Created for use with standard Unity planes.
/// </summary>
/// <remarks>
/// This is not a lookAt script. The plane maintains it's rotation even if the camera changes it's
/// position. The billboard is changed according to camera's rotation.
/// </remarks>
public class BillboardSimple : MonoBehaviour {

    public Vector3 baseRotation;

    //void OnValidate()
    //{
    //    // The -90 rotation is required for the script to work with Unity generated planes
    //    transform.rotation = Camera.main.transform.rotation * Quaternion.Euler(baseRotation);
    //}

    void FixedUpdate ()
    {
        // The -90 rotation is required for the script to work with Unity generated planes
        transform.rotation = Camera.main.transform.rotation * Quaternion.Euler(baseRotation);
        //Quaternion targetRotation = Camera.main.transform.rotation * Quaternion.Euler(baseRotation);
        //transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 1000f * Time.deltaTime);
    }
}
