﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicWalls : MonoBehaviour
{
    public GameObject player;
    private RaycastHit[] hitObjects;
    private MeshRenderer[] meshes;
    private List<RaycastHit> objectContainer;

    private LayerMask wallsOnly;

    //private CameraRotation cameraRotation;

    void Start()
    {
        objectContainer = new List<RaycastHit>();
        wallsOnly = LayerMask.GetMask("Walls");
        //cameraRotation = FindObjectOfType<CameraRotation>();
    }

    private void Update()
    {
        hitObjects = Physics.RaycastAll(Camera.main.transform.position - new Vector3(0f, 2f), Camera.main.transform.forward, Vector3.Distance(Camera.main.transform.position, player.transform.position) - 1f, wallsOnly);
        Debug.DrawRay(Camera.main.transform.position - new Vector3(0f, 2f), Camera.main.transform.forward * Vector3.Distance(Camera.main.transform.position, player.transform.position));
        foreach (RaycastHit objectInWay in hitObjects)
        {
            // && (objectInWay.transform.rotation.z == Camera.main.transform.rotation.z || objectInWay.transform.rotation.z == Camera.main.transform.rotation.z - 180f)
            if (!objectContainer.Contains(objectInWay) )
            {
                objectContainer.Add(objectInWay);
                meshes = objectInWay.transform.gameObject.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer wallMesh in meshes)
                {
                    wallMesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                }
            }
            //objectInWay.transform.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        }

        for(int i = 0; i < objectContainer.Count; i++)
        {
            bool isHit = false;
            foreach(RaycastHit hitObj in hitObjects)
            {
                if(hitObj.transform == objectContainer[i].transform)
                {
                    isHit = true;
                    break;
                }
            }

            if(!isHit)
            {
                meshes = objectContainer[i].transform.gameObject.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer wallMesh in meshes)
                {
                    wallMesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                }
                objectContainer.Remove(objectContainer[i]);
                i--;
            }
        }
    }

}
