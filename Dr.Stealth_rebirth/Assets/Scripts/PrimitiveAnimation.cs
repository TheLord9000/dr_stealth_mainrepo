﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimitiveAnimation : MonoBehaviour {

    public float animSensitivity = 0.1f;

    private GameObject cam;
    public float framesPerSecond;

    [Header("Idle animation")]
    public Texture idleA;
    public Texture idleB;
    public int columnsIdle;
    public int rowsIdle;
    public int framesIdle;

    [Header("Walk left/right animation")]
    public Texture walkLR;
    public int columnsWalkLR;
    public int rowsWalkLR;
    public int framesWalkLR;

    [Header("Walk up animation")]
    public Texture walkU;
    public int columnsWalkU;
    public int rowsWalkU;
    public int framesWalkU;

    [Header("Walk down animation")]
    public Texture walkD;
    public int columnsWalkD;
    public int rowsWalkD;
    public int framesWalkD;

    [Header("Sneak animation")]
    public Texture sneak;
    public int columnsSneak;
    public int rowsSneak;
    public int framesSneak;

    [Header("Run animation")]
    public Texture run;
    public int columnsRun;
    public int rowsRun;
    public int framesRun;

    private bool lastWasRight = true;
    private int limit;
    private int index;
    private int columns;
    private int rows;
    private Material mat;
    private Rigidbody playerRb;
    private PlayerController pc;

	void Start ()
    {
        index = 0;
        pc = GetComponentInParent<PlayerController>();
        limit = framesIdle;
        columns = columnsIdle;
        rows = rowsIdle;
        playerRb = GetComponentInParent<Rigidbody>();
        mat = GetComponent<Renderer>().sharedMaterial;
        StartCoroutine("updateTiling");
        cam = GameObject.FindGameObjectsWithTag("camera")[0];
        //  Use this if your plane doesn't have pre set UV maps for texture size.

    }

    private IEnumerator updateTiling()
    {
        while (true)
        {

            Vector3 relativeVect = playerRb.velocity; // To be modified
            int angle;
            if (cam == null || cam.GetComponent<CameraRotation>() == null)
            {
                angle = 0;
            }
            else
            {
                angle = cam.GetComponent<CameraRotation>().angle;
            }
            //Debug.Log(relativeVect);
            mat = GetComponent<Renderer>().sharedMaterial;
           
            bool right = false;                   
            bool left = false;
            bool up = false;
            bool down = false;
            switch (angle)
            {
                case 360:
                case 0:
                    right = relativeVect.x > animSensitivity;
                    left = relativeVect.x < -animSensitivity;
                    down = relativeVect.z > animSensitivity;
                    up = relativeVect.z < -animSensitivity;
                    break;
                case 90:
                    right = relativeVect.z < -animSensitivity;
                    left = relativeVect.z > animSensitivity;
                    up = relativeVect.x < -animSensitivity;
                    down = relativeVect.x > animSensitivity;
                    break;
                case 180:
                    left = relativeVect.x > animSensitivity;
                    right = relativeVect.x < -animSensitivity;
                    down = relativeVect.z < -animSensitivity;
                    up = relativeVect.z > animSensitivity;
                    break;
                case 270:
                    right = relativeVect.z > animSensitivity;
                    left = relativeVect.z < -animSensitivity;
                    up = relativeVect.x > animSensitivity;
                    down = relativeVect.x < -animSensitivity;
                    break;
                default:
                    break;
            }

            if (right)
            {
                if (!lastWasRight) transform.localScale = Vector3.Scale(transform.localScale, new Vector3(-1f, 1f, 1f));
                mat.mainTexture = walkLR;
                columns = columnsWalkLR;
                rows = rowsWalkLR;
                limit = framesWalkLR;
                if (Mathf.Abs(relativeVect.x) > pc._walkSpeed + 0.1f || Mathf.Abs(relativeVect.z) > pc._walkSpeed + 0.1f)
                {
                    mat.mainTexture = run;
                    columns = columnsRun;
                    rows = rowsRun;
                    limit = framesRun;
                }
                if (Input.GetAxis("Sneak") == 1 && Mathf.Abs(relativeVect.x) <= pc._walkSpeed)
                {
                    mat.mainTexture = sneak;
                    columns = columnsSneak;
                    rows = rowsSneak;
                    limit = framesSneak;
                }
                lastWasRight = true;
            }
            else if (left)
            {
                if (lastWasRight) transform.localScale = Vector3.Scale(transform.localScale, new Vector3(-1f, 1f, 1f));
                mat.mainTexture = walkLR;
                columns = columnsWalkLR;
                rows = rowsWalkLR;
                limit = framesWalkLR;
                if (Mathf.Abs(relativeVect.x) > pc._walkSpeed + 0.1f || Mathf.Abs(relativeVect.z) > pc._walkSpeed + 0.1f)
                {
                    mat.mainTexture = run;
                    columns = columnsRun;
                    rows = rowsRun;
                    limit = framesRun;
                }
                if (Input.GetAxis("Sneak") == 1 && Mathf.Abs(relativeVect.x) <= pc._walkSpeed)
                {
                    mat.mainTexture = sneak;
                    columns = columnsSneak;
                    rows = rowsSneak;
                    limit = framesSneak;
                }
                lastWasRight = false;
            }
            else if (up)
            {
                mat.mainTexture = walkU;
                columns = columnsWalkU;
                rows = rowsWalkU;
                limit = framesWalkU;
            }
            else if (down)
            {
                mat.mainTexture = walkD;
                columns = columnsWalkD;
                rows = rowsWalkD;
                limit = framesWalkD;
            }
            else
            {
                mat.mainTexture = idleA;
                columns = columnsIdle;
                rows = rowsIdle;
                limit = framesIdle;
            }

            index++;

            if (index >= limit)
            {
                index = 0;
            }

            Vector2 offset = new Vector2((float)index / columns - (index / columns), //x index
                                          (index / columns) / (float)rows);          //y index

            GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);
            Vector2 size = new Vector2(1f / columns, 1f / rows);
            GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", size);
            yield return new WaitForSeconds(1f / framesPerSecond);
        }
    }
}
