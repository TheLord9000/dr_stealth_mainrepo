﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrippyBackground : MonoBehaviour {

    private void Update()
    {
        Camera.main.backgroundColor = Color.Lerp(new Color(90, 49, 121), new Color(90, 29, 101), Mathf.PingPong(Time.time, 1));
    }
}
