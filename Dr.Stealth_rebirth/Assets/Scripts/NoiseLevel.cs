﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class NoiseLevel : MonoBehaviour {

    public enum Volume
    {
        none,
        silent,
        loud,
    }

    public Volume noiseLevel;
    
}
