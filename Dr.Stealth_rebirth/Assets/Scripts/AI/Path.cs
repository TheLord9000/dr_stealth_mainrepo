﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Path
{
    [SerializeField]
    public Waypoint[] waypoints;
    public Waypoint this[int i]
    {
        get => waypoints[i];
    }
}
