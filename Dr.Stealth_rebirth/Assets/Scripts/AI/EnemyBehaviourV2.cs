﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static _UsefulThings;
using System;


public class EnemyBehaviourV2 : MonoBehaviour
{
    public delegate void MovementDelegate();
    /// <summary>
    /// Enemy's behaviour state
    /// </summary>
    public enum State
    {
        normal,     // don't know about player  | -> check
        alert,      // noticed somethink        | -> suspicious, chase
        suspicious, // on sighting spot         | -> allert, normal
        chase,      // see plyer                | -> patrol
        patrol,     // had seen player          | -> chase
    }

    [Header("General")]
    public EnabledFeatures enabledFeatures;
    public float timeToAllarmOthers;
    [SerializeField]
    private State state = State.normal;

    [Header("Movement")]
    MovementDelegate Movement;
    [Range(0, 5)]
    public float waypointTolerance;
    public Paths paths;
    public Waypoint sightingSpot = null;

    private NavMeshAgent navAgent;

    private event Action SwitchToNormal;
    private event Action SwitchToAlert;
    private event Action SwitchToSuspicious;
    private event Action SwitchToChase;
    private event Action SwitchToPatrol;

    /// <summary>
    /// get => current enemy state
    /// set => set enemy state including behaviour change
    /// </summary>
    public State CurrentState
    {
        get => state;
        set
        {
            state = value;
            switch (value)
            {
                case State.normal:
                    SwitchToNormal();
                    Debug.Log("State switched to Normal");
                    return;

                case State.alert:
                    SwitchToAlert();
                    Debug.Log("State switched to Alert");
                    return;

                case State.suspicious:
                    SwitchToSuspicious();
                    Debug.Log("State switched to Suspicious");
                    return;

                case State.chase:
                    SwitchToChase();
                    Debug.Log("State switched to Chase");
                    return;

                case State.patrol:
                    SwitchToPatrol();
                    Debug.Log("State switched to Patrol");
                    return;
            }
        }
    }


    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        Movement = StandardMovement;
        paths.Initialize();

        _MainEventManager.plyerDetected += () => SwitchToChase();

        SwitchToNormal += () => state = State.normal;
        SwitchToNormal += () => navAgent.SetDestination(paths.CurrentWaypoint.transform.position);
        SwitchToNormal += () => Movement = StandardMovement;

        SwitchToAlert += () => state = State.alert;
        SwitchToAlert += () => Movement = AllertMovement;

        SwitchToSuspicious += () => state = State.suspicious;
        SwitchToSuspicious += () => Movement = SuspiciousMovement;

        SwitchToChase += () => state = State.chase;
        SwitchToChase += () => Movement = ChaseMovement;
        SwitchToChase += () => _MainEventManager.RaisePlyerDetectedEvent( timeToAllarmOthers );

        SwitchToPatrol += () => state = State.patrol;
        SwitchToPatrol += () => Movement = PatrolMovement;

    }

    private void Start()
    {
        SwitchToNormal();
    }

    private void Update()
    {
        // Uncomment t change state from inspector
        CurrentState = state;

        Movement();
    }

    void StandardMovement()
    {
        DoIfPlayerInLocation(paths.CurrentWaypoint, () =>
        {
            Debug.Log("Is waiting");
            paths.actuallWaitingTime -= Time.deltaTime;

            if (paths.actuallWaitingTime < 0)
            {
                paths.SetNextWaypoint();
                navAgent.SetDestination(paths.CurrentWaypoint.transform.position);
            }
        });
    }

    void AllertMovement()
    {
        navAgent.SetDestination(sightingSpot.transform.position);

        DoIfPlayerInLocation(sightingSpot, () => SwitchToSuspicious() );
    }
    

    void SuspiciousMovement()
    {
        DoIfPlayerInLocation(sightingSpot, () =>
        {
            SuspiciousMovementWaitTime.Value -= Time.deltaTime;

            if (SuspiciousMovementWaitTime.Value < 0)
            {
                SuspiciousMovementCounter.Value++;

                do
                {
                    sightingSpot.transform.position = RandomOffset(transform.position, waypointTolerance + 0.5f, Math.Max(waypointTolerance * 2, waypointTolerance + 1));
                    Debug.Log(navAgent.CalculatePath(sightingSpot.transform.position, new NavMeshPath()));
                }
                while( !navAgent.CalculatePath( sightingSpot.transform.position, new NavMeshPath() ) );

                navAgent.SetDestination(sightingSpot.transform.position);
                SuspiciousMovementWaitTime.Reset();
                Debug.Log(SuspiciousMovementWaitTime.Value);
            }

            Debug.Log(SuspiciousMovementCounter);
        });

        if (SuspiciousMovementCounter.Value == SuspiciousMovementCounterLimiter)
        {
            SuspiciousMovementCounter.Reset();
            SwitchToNormal();
        }
    }
    public int SuspiciousMovementCounterLimiter = 3;
    /// <summary>Used in SuspiciousMovement()</summary>
    private Resetable<int> SuspiciousMovementCounter = new Resetable<int>(0);
    /// <summary>Used in SuspiciousMovement()</summary>
    [SerializeField]
    public Resetable<float> SuspiciousMovementWaitTime = new Resetable<float>(0.5f);

    void ChaseMovement()
    {
        throw new System.NotImplementedException();
    }
    void PatrolMovement()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Does [void action()] if enemy is near to [waypoint]
    /// </summary>
    /// <param name="waypoint">target waypoint</param>
    /// <param name="action">action to do</param>
    /// <returns>value of 'if' statement</returns>

    private bool DoIfPlayerInLocation( Waypoint waypoint, Action action )
    {
        if (Mathf.Abs(waypoint.transform.position.x - transform.position.x) < waypointTolerance
         && Mathf.Abs(waypoint.transform.position.z - transform.position.z) < waypointTolerance)
        {
            action();
            return true;
        }

        return false;
    }


    
    public class EnabledFeatures
    {
        public bool sightEnabled = true;
        public bool hearingEnabled = true;
        public bool movementEnabled = true;
    }
}

//--------------------------------------------------------------------------


