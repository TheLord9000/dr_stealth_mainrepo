﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Paths
{
    public float actuallWaitingTime;
    public Waypoint CurrentWaypoint
    {
        get => paths[currPathIndex].waypoints[currWaypointIndex];
        //set => CurrentWaipoint = value;
    }

    [SerializeField]
    private Path[] paths;

    private int currPathIndex;
    private int currWaypointIndex;

    void Awake()
    {
        if (paths.Length == 0)
            Debug.Log("Warning: element has 0 paths! ");
    }

    public Path this[int i]
    {
        get => paths[i];
    }

    public void SetNextWaypoint()
    {
        Debug.Log("SetNextWaypoint");
        currWaypointIndex++;
        if (currWaypointIndex >= paths[currPathIndex].waypoints.Length)
        {
            currWaypointIndex = 0;
            currPathIndex = _MainLevelManager.RNG.Next(0, paths.Length - 1);
        }
        actuallWaitingTime = CurrentWaypoint.WaitTime;
    }

    public void Initialize()
    {
        currPathIndex = _MainLevelManager.RNG.Next(0, paths.Length - 1);
        currWaypointIndex = 0;
    }
}