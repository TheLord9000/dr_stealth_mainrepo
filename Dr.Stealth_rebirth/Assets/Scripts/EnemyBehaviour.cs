﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// A basic anamy movment and pathfinding script.
/// </summary>
/// <remarks>
/// In future this script will be base for AI script
/// </remarks>
/// 


public class EnemyBehaviour : MonoBehaviour
{
    //-[ Types ]------------------------------------------------------------------------------------

    public enum State
    {
        normal,
        alerted,
        noticed,
        chasing
    }

    [System.Serializable]
    public class EnabledFeatures
    {
        public bool sightEnabled = true;
        public bool hearingEnabled = true;
        public bool movementEnabled = true;
    }

    [System.Serializable]
    public class Path
    {
        [SerializeField]
        public int[] waypointsNumbers;
    }

    // IN INSPECTOR:

    [Header("General")]
        public GameObject levelControler;
        public EnabledFeatures enabledFeatures;
        public float timeToAllarmOthers;
        public bool silent = true;

    [SerializeField]
        private State state = State.normal;


    [Header("Sight")]
        public float vievDistance;
        [Range(0, 180)]
        public float FOV;
        public float detectionTime = 0.5f;

    [Header("Hearing")]
        public float hearingDistance;
        [SerializeField]
        private float realHearingDistance;
        [Range(0, 1)]
        public float silentModeMultiplier = 0.333F;
        [Range(1, 10)]
        public float loudModeMultiplier = 3;

    [Header("Movement")]
        private Vector3 currentWaypointCoordinates;
        [SerializeField]
        public Path[] paths;
        public Waypoint[] waypoints;
        [Range(0, 5)]
        public float waypointTolerance;

    // HIDDEN:

        private GameObject waypointPrefab;

    // Movement
        private NavMeshAgent navAgent;
        private int currentPath;
        private int currentWaypoint;
        private float actuallWaitingTime = -99999;
        private Waypoint hearedSomething;
        private Waypoint sawPlayer;

    // Sight
        private GameObject player;
        private Vector3 vectorToPlayer;
        private Collider playerCollider;
        private Light linesOfSight;
        private float timeToDetect;

    // Hearing
    private NoiseLevel playerNoiseScript;

    // Misc
        private AudioSource creepy;

    //-[ Start & Update Functions ]-----------------------------------------------------------------------------------

    void Start ()
    {
        // preparing movement
        creepy = GetComponent<AudioSource>();
        navAgent = GetComponent<NavMeshAgent>();
        waypointPrefab = levelControler.GetComponent<_MainLevelManager>().waypointPrefab.gameObject;

        SelectPath();
        currentWaypoint = 0;
        GetCurrentWaypoint().GenerateWaitTime();
        transform.position = GetCurrentWaypoint().transform.position;      


        //preparing hearing

        player = levelControler.GetComponent<_MainLevelManager>().player;
        playerNoiseScript = player.GetComponent<NoiseLevel>();
        
        //preparing sight

        playerCollider = player.GetComponent<Collider>();
        linesOfSight = GetComponentInChildren<Light>();
    }

    void FixedUpdate ()
    {        
        if (enabledFeatures.sightEnabled) Sight();
        if (enabledFeatures.hearingEnabled) Hearing();
        if (enabledFeatures.movementEnabled) Movement();

        if (Vector3.Distance(transform.position, player.transform.position) < waypointTolerance) GameOver.GAMEOVER();

        SightIndicator();        
    }

    //-[ Other Functions ]------------------------------------------------------------------------------------
    
    private void _print(string x)
    {
        if (!silent) print(x);
    }

    private void Movement()
    {
        switch( state )
        {
            case State.normal:
                if (Mathf.Abs(GetCurrentWaypoint().transform.position.x - transform.position.x) < waypointTolerance
                 && Mathf.Abs(GetCurrentWaypoint().transform.position.z - transform.position.z) < waypointTolerance)
                {
                    if (actuallWaitingTime < 0)
                    {
                        //_print(name + " in waypoint " + currentWaypoint);

                        actuallWaitingTime = GetCurrentWaypoint().GetWaitTime();

                        //_print(name + " is waiting " + actuallWaitingTime + " seconds in point " + currentWaypoint);
                    }

                    actuallWaitingTime -= Time.deltaTime;

                    if (actuallWaitingTime < 0)
                    {
                        SetNewPoint();
                        // _print(name + " new waypoint: " + currentWaypoint);
                    }
                }
                currentWaypointCoordinates = GetCurrentWaypoint().transform.position;
                break;

            case State.alerted:
                if(hearedSomething != null)
                {
                    ArrivingToPlayerPosProcedure(hearedSomething);
                }
                else
                {
                    state = State.normal;
                }
                break;

            case State.noticed:                
                if (sawPlayer != null)
                {
                    ArrivingToPlayerPosProcedure(sawPlayer);
                }
                else
                {
                    if (hearedSomething != null)
                    {
                        state = State.alerted;
                    }
                    else
                    {
                        state = State.normal;
                    }
                }
                break;

            case State.chasing:
                if( sawPlayer != null )
                {
                    if(!creepy.isPlaying) creepy.Play();
                    currentWaypointCoordinates = sawPlayer.transform.position;

                    ArrivingToPlayerPosProcedure(sawPlayer);
                }
                else
                {
                    if (creepy.isPlaying) creepy.Pause();

                    if (hearedSomething != null)
                    {
                        goto case State.alerted;
                    }
                    else
                    {
                        goto case State.normal;
                    }
                }
                break;
        }
        navAgent.SetDestination(currentWaypointCoordinates);
    }

    private void Sight()
    {
        linesOfSight.range = vievDistance;
        linesOfSight.spotAngle = FOV;

        RaycastHit hit;
        vectorToPlayer = player.transform.position - transform.position;
        bool playerNotHidden = Physics.Raycast( transform.position , vectorToPlayer , out hit , vievDistance );
        float angle = Vector3.Angle(vectorToPlayer, transform.forward);

        if (playerNotHidden && (angle < FOV/2 ) && hit.collider == playerCollider)
        {
            _print(name + " see player!!!");

            if (sawPlayer == null)
            {
                GameObject playerPosition = Instantiate(waypointPrefab);
                sawPlayer = playerPosition.GetComponent<Waypoint>();

                if(state!=State.chasing)
                    timeToDetect = detectionTime;
            }

            if ( timeToDetect < 0 )
            {
                sawPlayer.Set(player.transform.position, Color.red);
                //state = State.chasing;
                if (state != State.chasing) StartCoroutine("RaiseAllarm");
            }
            else 
            {
                timeToDetect -= Time.deltaTime;
                sawPlayer.Set(player.transform.position, new Color(1f,0.5f,0f) );

                state = State.noticed;
            }
        }
    }

    private void Hearing()
    {
        float distanceToPleyer = Vector3.Distance(transform.position, player.transform.position);
        NoiseLevel.Volume playerNoiseLevel = playerNoiseScript.noiseLevel;

        if ( playerNoiseLevel == NoiseLevel.Volume.none ) return;
        RaycastHit[] hits;
        hits = Physics.RaycastAll( transform.position, vectorToPlayer, distanceToPleyer );

        int obsticlesOnWay = hits.Length - 1;


        realHearingDistance = hearingDistance;
        if  ( playerNoiseLevel == NoiseLevel.Volume.silent) realHearingDistance *= silentModeMultiplier;
        else if (playerNoiseLevel == NoiseLevel.Volume.loud) realHearingDistance *= loudModeMultiplier;

        realHearingDistance *= Mathf.Pow( (1f/3f) , obsticlesOnWay );

        if ( distanceToPleyer < realHearingDistance )
        {
            if (state != State.chasing && state != State.noticed ) state = State.alerted;

            if (hearedSomething != null)
                Destroy(hearedSomething.gameObject);

            GameObject playerPosition = Instantiate(waypointPrefab);
            hearedSomething = playerPosition.GetComponent<Waypoint>();
            hearedSomething.Set(player.transform.position, Color.yellow );
        }
    }

    void SightIndicator()
    {
        switch(state)
        {
            case State.normal:
                linesOfSight.color = Color.green;
                break;
            case State.alerted:
                linesOfSight.color = Color.yellow;
                break;
            case State.noticed:
                linesOfSight.color = new Color(1f, 0.5f, 0f);//orange
                break;
            case State.chasing:
                linesOfSight.color = Color.red;
                break;
        }
    }

    private void SetNewPoint()
    {
        if( state != State.chasing)
        {
            currentWaypoint++;
            if (currentWaypoint == paths[currentPath].waypointsNumbers.Length)
            {
                SelectPath();
                currentWaypoint = 0;
            }
            GetCurrentWaypoint().GenerateWaitTime();
        }
        else
        {
            currentWaypoint = (int)Random.Range(0, levelControler.GetComponent<_MainLevelManager>().patrolPoints.Length);
        }

    }

    private void SelectPath()
    {
        currentPath = (int)Random.Range(0, paths.Length);
    }

    private Waypoint GetCurrentWaypoint()
    {
        if( state != State.chasing )
        {
            return waypoints[paths[currentPath].waypointsNumbers[currentWaypoint]];
        }
        else
        {
            return levelControler.GetComponent<_MainLevelManager>().patrolPoints[currentWaypoint];
        }
    }

    private void ArrivingToPlayerPosProcedure( Waypoint waypoint)
    {
        currentWaypointCoordinates = waypoint.transform.position;

        if (Mathf.Abs(transform.position.x - waypoint.transform.position.x) < waypointTolerance
        &&  Mathf.Abs(transform.position.z - waypoint.transform.position.z) < waypointTolerance)
        {
            Destroy(waypoint.gameObject);
            waypoint = null;

            if (state != State.chasing) state = State.normal;
        }
    }

    private IEnumerator RaiseAllarm()
    {
        Debug.Log("ALARM!!!!");
        Allarm();

        yield return new WaitForSeconds(timeToAllarmOthers);

        levelControler.GetComponent<_MainLevelManager>().AllarmAllEnemies();
    }

    public void Allarm()
    {
        actuallWaitingTime = 0;
        state = State.chasing;
        SetNewPoint();
    }
}


