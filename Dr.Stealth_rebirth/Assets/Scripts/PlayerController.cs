﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/// <summary>
/// Simple character movement, relative to the camera rotation.
/// </summary>
/// <remarks>
/// The only confusing thing for beginners may be the camera vector normalization.
/// I also used the Range for the fancy slider in the inspector.
/// </remarks>
public class PlayerController : MonoBehaviour {

    public bool movementEnabled = true;

    [Range(.1f, 15f)]
    public float _walkSpeed = 4f;

    [Range(.1f, 5f)]
    public float _sneakSpeed = 2f;

    [Range(2f, 30f)]
    public float _runSpeed = 8f;

    private bool isSneaking = false;
    private float verticalAxis, horizontalAxis;
    private Rigidbody rb;

    public InteractiveObject[] inventory = new InteractiveObject[5];
    public InteractiveObject chosenObject = null;
    private int chosenObjectIndex = -1;
    private Text itemInfo;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (movementEnabled)
        {
            verticalAxis = Input.GetAxis("Vertical");
            horizontalAxis = Input.GetAxis("Horizontal");

            Vector3 camForward = Camera.main.transform.forward;
            Vector3 camRight = Camera.main.transform.right;

            camForward.y = 0f;
            camRight.y = 0f;
            camForward.Normalize();
            camRight.Normalize();


            // Item interaction section =============================================

            MyGUI gui = FindObjectOfType<MyGUI>();
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (inventory[0] != null)
                {
                    chosenObject = inventory[0];
                    chosenObjectIndex = 0;

                    gui.chosenObject.text = "chosen object is " + chosenObject.name;
                }
                else
                {
                    gui.chosenObject.text = "inventory[0] not exists";
                    Debug.Log("no item in iventory on 0 position");
                }
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (inventory[1] != null)
                {
                    chosenObject = inventory[1];
                    chosenObjectIndex = 1;
                    gui.chosenObject.text = "chosen object is " + chosenObject.name;
                }
                else
                {
                    gui.chosenObject.text = "inventory[1] not exists";
                    Debug.Log("no item in iventory on 1 position");
                }
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                chosenObject = null;
                chosenObjectIndex = -1;
                gui.chosenObject.text = "chosen item cleaned";
            }

            // Item interaction section end ==========================================

            // Movement section ======================================================

            Vector3 movementDirection = verticalAxis * camForward + horizontalAxis * camRight;
            float multiplier;

            if (Input.GetKey(KeyCode.LeftShift))
            {
                multiplier = _runSpeed;
                isSneaking = false;
            }
            else if (Input.GetAxis("Sneak") == 1)
            {
                multiplier = _sneakSpeed;
                isSneaking = true;
            }
            else
            {
                multiplier = _walkSpeed;
                isSneaking = false;
            }

            rb.velocity = (movementDirection * multiplier);
            NoiseLevel level = GetComponent<NoiseLevel>();

            if (rb.velocity == Vector3.zero || isSneaking == true)
            {
                level.noiseLevel = NoiseLevel.Volume.none;
            }
            else if (rb.velocity.magnitude <= _walkSpeed+2)// +2 zabespiecza przed włączaniem loud podczas chodu na skos - 1 nie wystarcza xD
            {
                level.noiseLevel = NoiseLevel.Volume.silent;
            }
            else
            {
                level.noiseLevel = NoiseLevel.Volume.loud;
            }

            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100))
                {
                    Debug.Log(hit);
                    InteractiveObject interactiveObject = hit.collider.gameObject.GetComponent<InteractiveObject>();
                    Debug.Log(interactiveObject);
                    if (interactiveObject != null && interactiveObject.pickable)
                    {
                        Debug.Log("ok");
                        Debug.Log(PickItem(interactiveObject));
                        //Debug.Log(inventory.ToArray().Le)
                    }
                    else if (interactiveObject != null && !interactiveObject.pickable && chosenObject != null)
                    {
                        Debug.Log("try to use " + chosenObject.Name + " on " + interactiveObject.Name);
                        if (chosenObject.Use(interactiveObject))
                        {
                            inventory[chosenObjectIndex] = null;
                            chosenObjectIndex = -1;
                            chosenObject = null;
                        }
                    }
                    else if (interactiveObject != null && !interactiveObject.pickable && chosenObject == null)
                    {
                        gui.usedObject.text = "You don't have object to use with " + interactiveObject.Name;
                        Debug.Log("try to find sth to use with " + interactiveObject.Name);
                    }


                }
            }
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }
    public bool PickItem(InteractiveObject interactive)
    {
        float distanceToPleyer = Vector3.Distance(interactive.gameObject.transform.position, transform.position);
        if (distanceToPleyer > 10)
        {
            Debug.Log(distanceToPleyer);
            /**
             *@todo display message that object is to far 
             */
            Debug.Log("to far");
            return false;
        }

        if (interactive.pickable && !interactive.used && !interactive.pickedUp)
        {
            interactive.pickedUp = true;
            for (int i = 0; i < inventory.Length; i++)
            {
                if (inventory[i] == null)
                {
                    inventory[i] = interactive;
                    break;
                }
            }
            MyGUI gui = FindObjectOfType<MyGUI>();
            gui.whatGet.text = interactive.name + " added to inventory";


            Debug.Log(interactive.name + " added to inventory");
            interactive.gameObject.transform.position = new Vector3(0, -10000, 0);
            return true;
        }
        return false;
    }

}
