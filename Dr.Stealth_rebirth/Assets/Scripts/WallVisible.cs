﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallVisible : MonoBehaviour {

    public int HiddenWhenAngleEquals=0;

    private CameraRotation cameraRotation;
	// Use this for initialization
	void Start ()
    {
        cameraRotation = FindObjectOfType<CameraRotation>();
	}
	
	// Update is called once per frame
	void Update () {
        MeshRenderer MeshComponent = gameObject.GetComponent<MeshRenderer>();
        if (HiddenWhenAngleEquals == cameraRotation.angle || HiddenWhenAngleEquals == (cameraRotation.angle-180) || HiddenWhenAngleEquals == (cameraRotation.angle - 360))
        {
            MeshComponent.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        }
        else
        {
            MeshComponent.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        }
	}
}
