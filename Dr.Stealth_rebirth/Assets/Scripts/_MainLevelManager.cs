using System.Collections;
using UnityEngine;

public class _MainLevelManager : MonoBehaviour {

    public static _MainLevelManager instance;

    private int _currentScore = 0;
    /// <summary>
    /// Global random number generator
    /// </summary>
    public static RandomPlus RNG = new RandomPlus();

    public GameObject waypointPrefab;

    public GameObject player;
    public GameObject[] enemies;
    public Waypoint[] patrolPoints;

    private void Awake()
    {
        // If an instance already exists, destroy this one
        if (instance != null)
            Destroy(this.gameObject);
        // Otherwise, make this the instance
        else
            instance = this;

        // Enable persistence across scenes
        DontDestroyOnLoad(this.gameObject);
    }

    public int AdjustScore(int add)
    {
        _currentScore += add;
        return _currentScore;
    }

    public void AllarmAllEnemies()
    {
        foreach( GameObject enemy in enemies )
        {
            enemy.GetComponent<EnemyBehaviour>().Allarm();
        }
    }
    /// <summary>
    /// Extends System.Random
    /// </summary>
    public class RandomPlus : System.Random
    {
        public double NextDouble( double min, double max )
        {
            return ((NextDouble() * 2 - 1.0) * (max - min)) + max;
        }
    }
}
