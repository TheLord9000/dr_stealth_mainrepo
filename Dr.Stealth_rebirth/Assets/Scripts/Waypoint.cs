using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Waypoint : MonoBehaviour {

    public float minWaitTime = 0;
    public float maxWaitTime = 0;
    public bool invisible = false;

    public static bool seeDetectionPoints;

    private float waitTime;
    private LineRenderer lineRenderer;


    public void Set( Vector3 position , Color color)
    {
        transform.position = position;
        minWaitTime = 0;
        maxWaitTime = 0;


        LineRenderer renderer = GetComponent<LineRenderer>();
        if ( seeDetectionPoints )
        {
            renderer.startColor = color;
            renderer.endColor = color;
            renderer.enabled = true;
        }
        else
        {
            renderer.enabled = false;
        }
    }

    [Obsolete("Use accesor \"WaitTime\"")]
    public void GenerateWaitTime()
    {
        waitTime = UnityEngine.Random.Range(minWaitTime, maxWaitTime);
    }

    [Obsolete("Use accesor \"WaitTime\"")]
    public float GetWaitTime()
    {
        return waitTime;
    }

    public float WaitTime { get => UnityEngine.Random.Range(minWaitTime, maxWaitTime); }

    private void Start()
    {
        if(invisible == true)
        {
            if (maxWaitTime < minWaitTime) throw new MaxLowerThanMinWaittimeException();

            this.GetComponent<LineRenderer>().enabled = false;
        }
    }

    class MaxLowerThanMinWaittimeException : System.Exception { };

}
