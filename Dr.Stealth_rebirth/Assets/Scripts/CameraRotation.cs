﻿using UnityEngine;

/// <summary>
/// Ortographic camera fixed interval rotation script.
/// Should be a component of CameraController, because it doesn't include any offset functionality
/// </summary>
/// <remarks>
/// The offset is achieved by parenting the MainCamera to CameraControllerr, which acts as a pivot
/// of rotation. This is a much simpler method than translating the rotation inside the script.
/// I couldn't get the GetAxis to work properly. This action requires the GetKeyDown input to work,
/// otherwise it's broken. This is a TO DO thing, if we want controller support in the future.
/// </remarks>

public class CameraRotation : MonoBehaviour {

    public float _speed = 1f;
    private Quaternion targetRotation;
    public int angle = 0;
    public int rotationAngle = 90;
    private bool activeKey = false;

    private void Start()
    {
        targetRotation = transform.rotation;
    }

    void Update ()
    {
		if(Input.GetAxis("CameraRotation")>0 && !activeKey)
        {
            activeKey = true;
            angle += 90;
            if (angle >= 360)
            {
                angle -= 360; 
            }
            targetRotation *= Quaternion.AngleAxis(rotationAngle, Vector3.up);
            Debug.Log(targetRotation);
            Debug.Log(angle);
        }
        else if(Input.GetAxis("CameraRotation") < 0 && !activeKey)
        {
            activeKey = true;
            angle -= 90;
            if (angle <= 0)
            {
                angle += 360;
            }
            targetRotation *= Quaternion.AngleAxis(-rotationAngle, Vector3.up);
            Debug.Log(targetRotation);
            Debug.Log(angle);
        }
        else if (Input.GetAxis("CameraRotation") == 0)
        {
            activeKey = false;
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, _speed * Time.deltaTime);

    }
}
