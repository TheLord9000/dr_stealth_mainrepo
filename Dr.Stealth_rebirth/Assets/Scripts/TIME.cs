﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TIME : MonoBehaviour {

    public TextMeshProUGUI timerText;

    private float timer = 0.0f;
    private float timerMax= 600.0f;

    public void UpdateLevelTimer(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);

        string formatedSeconds = seconds.ToString();

        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }

        timerText.text = minutes.ToString("00") + " " + seconds.ToString("00");
    }

    void Start()
    {
        timerText = GetComponent<TextMeshProUGUI>();
        timer = timerMax;
    }
    void Update()
    {
        timer -= Time.deltaTime;

        if (timer >= 0)
           UpdateLevelTimer(timer);
        else
        {
            Debug.Log("timer Zero reached !");
        }
    }
}
