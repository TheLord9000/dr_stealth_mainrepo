﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicTransparency : MonoBehaviour
{
    public GameObject player;
    [Range(0.0f, 1.0f)]
    public float transparency;

    private Vector3[] boundingBox;

    private List<RaycastHit> hitObjects;
    private MeshRenderer[] meshes;
    private List<RaycastHit> objectContainer;

    private LayerMask objectsOnly;

    //private CameraRotation cameraRotation;

    void Start()
    {
        hitObjects = new List<RaycastHit>();
        boundingBox = new Vector3[6] {
            new Vector3(-0.4f, 2f),
            new Vector3(0.4f, 2f),
            new Vector3(-0.4f, 0f),
            new Vector3(0.4f, 0f),
            new Vector3(-0.4f, -1f),
            new Vector3(0.4f, -1f)
        };
        objectContainer = new List<RaycastHit>();
        objectsOnly = LayerMask.GetMask("SeeThrough", "Door");
        //cameraRotation = FindObjectOfType<CameraRotation>();
    }

    private void Update()
    {
        for(int j = 0; j < boundingBox.Length; j++)
        {
            hitObjects.AddRange(Physics.RaycastAll(Camera.main.transform.position - boundingBox[j], Camera.main.transform.forward, Vector3.Distance(Camera.main.transform.position, player.transform.position) - 1f, objectsOnly));
            Debug.DrawRay(Camera.main.transform.position - boundingBox[j], Camera.main.transform.forward * (Vector3.Distance(Camera.main.transform.position, player.transform.position) - 1f));
        }
        foreach (RaycastHit objectInWay in hitObjects)
        {
            // && (objectInWay.transform.rotation.z == Camera.main.transform.rotation.z || objectInWay.transform.rotation.z == Camera.main.transform.rotation.z - 180f)
            if (!objectContainer.Contains(objectInWay))
            {
                objectContainer.Add(objectInWay);
                meshes = objectInWay.transform.gameObject.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer wallMesh in meshes)
                {
                    SetMaterialTransparent(wallMesh.material);
                    Color objColor = wallMesh.material.color;
                    objColor.a = transparency;
                    wallMesh.material.color = objColor;
                }
            }
            //objectInWay.transform.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        }


        for (int i = 0; i < objectContainer.Count; i++)
        {
            bool isHit = false;
            foreach (RaycastHit hitObj in hitObjects)
            {
                if (hitObj.transform == objectContainer[i].transform)
                {
                    isHit = true;
                    break;
                }
            }

            if (!isHit)
            {
                meshes = objectContainer[i].transform.gameObject.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer wallMesh in meshes)
                {
                    SetMaterialOpaque(wallMesh.material);
                    Color objColor = wallMesh.material.color;
                    objColor.a = 1.0f;
                    wallMesh.material.color = objColor;
                }
                objectContainer.Remove(objectContainer[i]);
                i--;
            }
        }

        hitObjects.Clear();
    }
   

    void SetMaterialTransparent(Material mat)
    {
        mat.SetFloat("_Mode", 2);
        mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        mat.SetInt("_ZWrite", 0);
        mat.DisableKeyword("_ALPHATEST_ON");
        mat.EnableKeyword("_ALPHABLEND_ON");
        mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        mat.renderQueue = 3000;
    }

    void SetMaterialOpaque(Material mat)
    {
        mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
        mat.SetInt("_ZWrite", 1);
        mat.DisableKeyword("_ALPHATEST_ON");
        mat.DisableKeyword("_ALPHABLEND_ON");
        mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        mat.renderQueue = -1;
    }

}
