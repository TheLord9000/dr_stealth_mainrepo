﻿using UnityEngine;

/// <summary>
/// Extremely simple camera follow script.
/// Attach it to the main camera (or it's parent).
/// </summary>

public class CameraFollowPlayer : MonoBehaviour {

    private GameObject player;

	void Start ()
    {
        player = GameObject.FindWithTag("Player");
	}
	
	void Update ()
    {
        transform.position = player.transform.position;
	}
}
