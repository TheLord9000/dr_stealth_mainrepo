﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public static class _UsefulThings
{

    public static Vector3 RandomOffset(Vector3 original, float minRange, float maxRnage)
    {
        Vector3 offset = new Vector3((float)_MainLevelManager.RNG.NextDouble(), 0, (float)_MainLevelManager.RNG.NextDouble());
        offset = offset.normalized * (float)_MainLevelManager.RNG.NextDouble(minRange, maxRnage);

        return original + offset;
    }

    [Serializable]
    public class Resetable<T>
    {
        [SerializeField]
        private T actual;
        private readonly T original;

        public Resetable(T initValue)
        {
            actual = original = initValue;
        }
        public Resetable(T value, T origin)
        {
            actual = value;
            original = origin;
        }

        public void Reset()
        {
            actual = original;
        }

        public T Value
        {
            get => actual;
            set => actual = value; 
        }

        public T Origin
        {
            get => original;
        }

    }

}
