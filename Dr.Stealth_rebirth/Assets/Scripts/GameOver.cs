﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOver : MonoBehaviour {

    public static TextMeshProUGUI GOtext;

    public static void GAMEOVER()
    {
        GOtext = GameObject.FindGameObjectWithTag("GameOverText").GetComponent<TextMeshProUGUI>();
        GOtext.enabled = true;
        Time.timeScale = 0;
    }
}
